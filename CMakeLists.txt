# CMake 最低版本号要求
cmake_minimum_required (VERSION 2.8)

# 项目信息
project (fb_test)

set(VERSION 1)
set(PATCHLEVEL 1)
set(SUBLEVEL 0)
set(EXTRAVERSION .git)
set(NAME rosetta)

# specify the cross compiler
SET(CMAKE_C_COMPILER   arm-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)

add_definitions(-DVERSION=${VERSION})
add_definitions(-DPATCHLEVEL=${PATCHLEVEL})
add_definitions(-DSUBLEVEL=${SUBLEVEL})
add_definitions(-DVERSION_NAME=\"${NAME}\")

# 添加 src 子目录
# add_subdirectory(src)

# 查找当前目录下的所有源文件
# 并将名称保存到 DIR_SRCS 变量
# aux_source_directory(. DIR_SRCS)

set(COMMON_SOURCES common.c font_8x8.c sync_cache.c)
set(FBTEST_SOURCES fb-test.c ${COMMON_SOURCES})
set(PERF_SOURCES perf.c ${COMMON_SOURCES})
set(RECT_SOURCES rect.c ${COMMON_SOURCES})
set(OFFSET_SOURCES offset.c ${COMMON_SOURCES})
set(FBSTRING_SOURCES fb-string.c ${COMMON_SOURCES})

# include_directories()
# link_directories()

# set (EXTRA_LIBS ${EXTRA_LIBS} blobmsg_json ubox json-c ubus pthread)

# 指定生成目标
add_executable(fbtest ${FBTEST_SOURCES})
add_executable(perf ${PERF_SOURCES})
add_executable(rect ${RECT_SOURCES})
add_executable(offset ${OFFSET_SOURCES})
add_executable(fbstring ${FBSTRING_SOURCES})

# target_link_libraries (${PROJECT_NAME} ${EXTRA_LIBS})
